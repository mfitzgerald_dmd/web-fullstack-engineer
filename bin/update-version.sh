#!/bin/sh

TARGET=$1
VERSION=$(git symbolic-ref -q --short HEAD || git describe --tags --exact-match)

ESCAPEDVERSION=$(echo $VERSION|sed -e 's/\//\\\//g');

`sed -i -e 's/"version": ".*",/"version": "'$ESCAPEDVERSION'",/' $TARGET`