import React from "react";
import { Container } from "semantic-ui-react";
import config from "../config/app";

export const Footer = () => {
    return (
        <Container
            style={{
                backgroundImage: `url(${config.assets.banner})`,
                backgroundSize: "contain",
                height: "auto"
            }}
        >
            <Container
                style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                }}
            >
                <h2 style={{ padding: "8px" }}>
                    <a
                        href={config.assets.privacy}
                        style={{ color: "#fff", textDecoration: "underline" }}
                    >
                        DMD Privacy Policy
                    </a>
                </h2>
            </Container>
        </Container>
    );
};

export default Footer;
