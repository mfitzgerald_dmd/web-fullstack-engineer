import React from "react";
import { Grid } from "semantic-ui-react";

export const EmailCopy = () => {
    return (
        <Grid centered={true}>
            <Grid.Row>
                <Grid.Column computer={16} mobile={16} stretched={true} floated="left">
                    <h2>Email Preferences</h2>

                    <h3>
                        What Amount of Medically Relevant Content Is Right for You?
                    </h3>

                    <p>It is DMD's desire that you receive communication that is relevant
                        to you. To ensure this, we have provided several selections below.
                    </p>

                    <p>By remaining subscribed to the DMD Healthcare Communications Network
                        you have email based access to current relevant medical communications
                        that include information from: Leading National Hospitals, Key Opinion Leaders,
                        Pharmaceutical Companies, Top Healthcare Recruiters, Relevant Medical Publications,
                        Paid Research Opportunities, Surveys, and Opportunities to earn Continuing Medical
                        Education Credits
                    </p>

                    <p>You are currently subscribed to the DMD Healthcare Communications
                        Network and are receiving information and opportunities via email
                        from:
                    </p>

                    <ul>
                        <li>Medical Publishers</li>
                        <li>Top National Hospitals</li>
                        <li>Leading Pharmaceutical and Medical Device Companies</li>
                        <li>Pharmaceutical Sample Opportunities</li>
                        <li>Pharmaceutical Recall Notifications</li>
                        <li>Relevant Job Opportunities</li>
                        <li>Research and Survey Opportunities</li>
                        <li>CME and eCME Opportunities</li>
                        <li>Information from Top Specialists and Thought Leaders in your Field</li>
                    </ul>

                    <p>DMD is committed to ensuring that information provided to you is
                        relevant to you, your practice, and your patients.
                    </p>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    );
};

export default EmailCopy;
