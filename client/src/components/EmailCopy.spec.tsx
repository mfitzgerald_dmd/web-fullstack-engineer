import React from "react";
import renderer from "react-test-renderer";
import { EmailCopy } from "./EmailCopy";

describe("EmailCopy [unit]", () => {
    it("renders as expected", () => {
        const tree = renderer.create(<EmailCopy />);
        expect(tree).toMatchSnapshot();
    });
});
