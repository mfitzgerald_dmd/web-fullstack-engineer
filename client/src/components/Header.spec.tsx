import React from "react";
import renderer from "react-test-renderer";
import { Header } from "./Header";

describe("Header [unit]", () => {
    it("renders as expected", () => {
        const tree = renderer.create(<Header />);
        expect(tree).toMatchSnapshot();
    });
});
