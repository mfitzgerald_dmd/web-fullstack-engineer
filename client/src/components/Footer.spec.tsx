import React from "react";
import renderer from "react-test-renderer";
import { Footer } from "./Footer";

describe("Footer [unit]", () => {
    it("renders as expected", () => {
        const tree = renderer.create(<Footer />);
        expect(tree).toMatchSnapshot();
    });
});
