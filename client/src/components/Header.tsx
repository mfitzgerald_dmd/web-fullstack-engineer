import React from "react";
import { Container, Image } from "semantic-ui-react";
import config from "../config/app";

export const Header = () => {
    return (
        <Container
            style={{
                backgroundImage: `url(${config.assets.banner})`,
                backgroundSize: "contain",
                height: "auto"
            }}
        >
            <Container>
                <Image
                    src={config.assets.logo}
                    style={{ padding: "8px" }}
                />
            </Container>
        </Container>
    );
};

export default Header;
