import React from "react";
import { Container } from "semantic-ui-react";
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import { EmailCopy } from "../components/EmailCopy";

class App extends React.Component {
    render() {
        return (
            <Container>
                <Header />
                <Container
                    style={{
                        padding: "26px",
                        display: "flex",
                        flexDirection: "column",
                        border: "4px solid #3264a9"
                    }}
                >
                    <EmailCopy />
                </Container>
                <Footer />
            </Container>
        );
    }
}
export default App;
