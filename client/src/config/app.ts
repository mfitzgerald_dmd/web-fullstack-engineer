const env = process.env.REACT_APP_ENV || "local";

const assets = {
    banner: "http://www.dmdconnects.com/hs-fs/hub/357405/file-1144153081-png/image/Header-bg.png", 
    logo: "http://www.dmdconnects.com/hs-fs/hub/357405/file-1144153081-png/image/Header-bg.png",
    privacy: "http://usersupport.dmdconnects.com/PrivacyPolicy.htm",
};

const test = {
    "assets": assets,
    "env" : env,
};

const local = {
    "assets": assets,
    "env" : env,
};

const dev = {
    "assets": assets,
    "env" : env,
};

const sbx = {
    "assets": assets,
    "env" : env,
};

const stg = {
    "assets": assets,
    "env" : env,
};

const qa = {
    "assets": assets,
    "env" : env,
};

const uat = {
    "assets": assets,
    "env" : env,
};

const prod = {
    "assets": assets,
    "env" : env,
};

export const config = {
    "dev": dev,
    "local": local,
    "prod": prod,
    "qa": qa,
    "sbx": sbx,
    "stg": stg,
    "test": test,
    "uat": uat,
};

export default config[env];
