# Usage

## Migrations

Run outstanding migrations

```
$ cd server
$ yarn run migrate:run
```

Add an additional migration

```
$ cd server
$ yarn run migrate:create AlterUsersTable
```

## Tests

Run tests directly on your laptop. 

You do not need to connect to the docker container to run tests.

Frontend Tests

```
$ cd client
$ yarn run test:unit
```

Backend Tests

```
$ cd server
$ yarn run test:functional
```

Note: TypeORM throws a warning that you should be able to ignore

```
console.warn node_modules/sql.js/js/sql.js:3
      writeStringToMemory is deprecated and should not be called! Use stringToUTF8() instead!
```
