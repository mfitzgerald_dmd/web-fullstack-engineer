# DMD Web - Full Stack Engineer Hiring Test

Single page React.js / Node.js app for proof of concepts

## Dependencies

Before starting please install node@8.10.0 and yarn@1.13.0

Note: Need help setting up node, yarn: see [Dependencies](./docs/01_dependencies.md)

## Installation

* Clone with `git`
* Run `yarn`
* Run migrations: [Usage](./docs/02_usage.md)
* start docker container [optional]

Note: Need help installing docker: see https://docs.docker.com/v17.12/docker-for-mac/install/

```
$ git clone [repo] 
$ cd client/
$ yarn install
$ cd ../server/
$ yarn install
$ yarn run migrate:run
$ cd ../
$ docker-compose up
```

Wait for the container to startup (first time startup is ~3min, subsequent container startups will be faster). After container starts, yarn will start the frontend and backend builds.

Docker Note:

Upon completion of the builds, the app will be accessible here [http://localhost:9001/](http://localhost:9001/) not localhost:3000 or localhost:3001

Using Docker is OPTIONAL. If you do not intend to use Docker, keep in mind the backend API will require CORS support.

## Expectations

On bitbucket.org, create a private fork of this repository to preserve your confidentiality. 

Create a feature branch on your private fork. Implement the frontend and backend requirements below. Out-of-box this project will have some defects in it, please fix them if they block you. Any blocking defects you resolve will demonstrate your problem solving ability. All files can be modified in this project. 

When ready, please submit a PR on your private fork between your private fork master branch and your private fork feature branch. Please provide mfitzgerald_dmd read access to your private forked repository so we can review your pr.

### Frontend 

* Implement this [Product Mock](./docs/03_product_mock.pdf)
* Use Semantic UI for React https://react.semantic-ui.com/
* Demonstrate knowledge of React.js
* Demonstrate knowledge of Redux.js
* Implement Jest based unit test coverage of React components
* [NICE TO HAVE] Adopt TypeScript as often as possible

### Backend

* Backend routes will be matched when the URL path starts with "/api"
* Use TypeORM and save the data to a sql.js database. http://typeorm.io/#/
* [NOTE] sql.js runs in memory and will not persist the data, you do not have to demonstrate reads of pre-existing data.
* Demonstrate knowledge of Express.js
* Implement Jest based unit test coverage 
* Implement Supertest based functional test coverage https://www.npmjs.com/package/supertest
* [NICE TO HAVE] Implement TypeScript as often as possible

## Docs

* [Localized Development](./docs/00_localized_development.md)
* [Dependencies](./docs/01_dependencies.md)
* [Usage](./docs/02_usage.md)
