import { Connection, createConnection } from "typeorm";
import { User } from "../entity/user";

export const EntityManager = (
    spec: {
        // tslint:disable-next-line:no-any
        "config": any,
    }) => {

    const { config } = spec;
    let connection: Promise<Connection>;

    async function connect() {
        const createConfig = {
            ...config,
            "entities": [
              User,
            ],
            "logging": false,
            "synchronize": false,
        };
        connection = createConnection(createConfig);
        return connection;
    }

    return {
        "getConnection": async (name: string) => {
            if (typeof connection === "undefined") {
                return connect();
            }
            return connection;
        },
    };
};

export {
    User,
};

export default EntityManager;
