import { Connection } from "typeorm";
import config from "../config/app";
import { EntityManager } from "./entity_manager";

describe("EntityManager", () => {

    describe("[functional]", () => {
        // tslint:disable-next-line:no-any
        let connection: any;
        // tslint:disable-next-line:no-any
        let entityManager: any;

        beforeAll(() => {
            entityManager = EntityManager({ "config": config.db });
        });

        afterAll(() => {
            connection.close();
        });
        test("EntityManager::getConnection", async () => {
            expect.assertions(1);
            connection = await entityManager.getConnection();
            expect(connection).toBeInstanceOf(Connection);
        });
    });
});
