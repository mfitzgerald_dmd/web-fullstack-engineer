import { validate } from "class-validator";
import config from "../config/app";
import { EntityManager } from "../dbal/entity_manager";
import { User } from "./user";

describe("User", () => {
    describe("[unit]", () => {
        test("User: Validation should fail [unit]", async () => {
            const user = new User();
            user.email = "f";
            const errors = await validate(user);
            expect(errors.length).toBe(1);
            expect(errors[0].constraints.isEmail).toBe("email must be an email");
        });

        test("User: Validation should pass [unit]", async () => {
            const user = new User();
            user.email = "mail@test.com";
            const errors = await validate(user);
            expect(errors.length).toBe(0);
        });
    });

    describe("[functional]", () => {
        // tslint:disable-next-line:no-any
        let connection: any;
        // tslint:disable-next-line:no-any
        let entityManager: any;

        beforeAll(() => {
            entityManager = EntityManager({ "config": config.db });
        });

        afterAll(() => {
            connection.close();
        });

        test("Test read, create, read", async () => {
            connection = await entityManager.getConnection();
            await connection.synchronize(true);
            const repository = connection.getRepository(User);
            const email = "mail@test.com";
            const result1: User = await repository.findOne({"email": email});
            expect(result1).toBeUndefined();
            const user = new User();
            user.email = email;
            await repository.save(user);
            const result2: User = await repository.findOne({"email": email});
            expect(result2).toMatchSnapshot();
        });
    });
});
