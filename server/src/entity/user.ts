import { IsEmail } from "class-validator";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("users")
export class User {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ 
        "name": "email"
    })
    @IsEmail()
    public email: string;

}
