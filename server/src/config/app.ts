import * as path from "path";

const env = process.env.NODE_ENV || "local";
const resources: string = path.resolve(__dirname, "../../../resources");

const db = {
    "location": `${resources}/sqljs/web_app.${env}.db`,
    "migrations": ["migration/*.js"],
    "migrationsRun": true,
    "migrationsTableName": "migrations",
    "synchronize": false,
    "type": "sqljs",
};

const test = {
    "db": db,
};

const local = {
    "db": db,
};

const sbx = {
    "db": db,
};

const dev = {
    "db": db,
};

const qa = {
    "db": db,
};

const uat = {
    "db": db,
};

const stg = {
    "db": db,
};

const prod = {
    "db": db,
};

const config = {
    "dev": dev,
    "local": local,
    "prod": prod,
    "qa": qa,
    "sbx": sbx,
    "stg": stg,
    "test": test,
    "uat": uat,
};

export default config[env];
