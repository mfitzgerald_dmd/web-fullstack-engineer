import * as bodyParser from "body-parser";
import * as express from "express";
import * as request from "supertest";
import { Connection } from "typeorm";
import config from "../config/app";
import { EntityManager } from "../dbal/entity_manager";
import { UserPreferencesHandler } from "../routes/user_preferences_handler";

async function getApp(connection: Connection) {
    const app = express();
    
    return app
            .use(bodyParser.urlencoded({ "extended": false }))
            .use(bodyParser.json())
            .use("/api/v1/user/preferences", UserPreferencesHandler(connection));
}

describe("POST /api/v1/user/preferences", () => {

    describe("[functional]", () => {

        const testPayload = {
            
        };

        // tslint:disable-next-line:no-any
        let connection: any;
        // tslint:disable-next-line:no-any
        let entityManager: any;

        beforeAll(() => {
            entityManager = EntityManager({ "config": config.db });
        });

        afterAll(() => {
            connection.close();
        });

        it("Invalid Method", async (done) => {
            connection = await entityManager.getConnection();
            const app = await getApp(connection);
            const res = await request(app)
                .get("/api/v1/user/preferences")
                .set("Accept", "/application\/json/")
                .set("X-Forwarded-For", "5.188.211.24");

            expect(res.status).toEqual(404);
            expect(res.get("Content-Type")).toEqual("text/html; charset=utf-8");
            done();
        });

        it("Invalid POST", async (done) => {
            connection = await entityManager.getConnection();
            const app = await getApp(connection);
            const res = await request(app)
                .post("/api/v1/user/preferences")
                .type("form")
                .set("Accept", "/application\/json/")
                .set("X-Forwarded-For", "5.188.211.24")
                .send(testPayload);

            expect(res.status).toEqual(200);
            expect(res.body.error).toEqual(null);
            expect(res.body.results).toEqual(null);
            expect(res.get("Content-Type")).toEqual("application/json; charset=utf-8");
            done();
        });
    });
});
