import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateUsersTable1555701851981 implements MigrationInterface {

    // tslint:disable-next-line:no-any
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE users(id INTEGER PRIMARY KEY ASC, email)`);
    }

    // tslint:disable-next-line:no-any
    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE users`);
    }
}
