import * as bodyParser from "body-parser";
import * as express from "express";

import config from "./config/app";
import { EntityManager } from "./dbal/entity_manager";
import { UserPreferencesHandler } from "./routes/user_preferences_handler";

const serverLogger = console;
const app = express();

const addRoutes = async () => {
    try {
        const entityManager = EntityManager({ "config": config.db });
        const connection = await entityManager.getConnection("default");
        app
            .use(bodyParser.urlencoded({ "extended": false }))
            .use(bodyParser.json())
            .use("/api/v1/user/preferences", UserPreferencesHandler(connection));
    
    } catch (err) {
       serverLogger.dir(err);
    }
};

addRoutes();

export default app;
